import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ContactoService } from '../../app/services/ContactoService';
import { ContactosPage } from './contactos';

@Component({
  selector: 'page-contacto-form',
  templateUrl: 'contacto-form.html',
})
export class ContactoFormPage implements OnInit {
  private parametro:string;
  private titulo:string;

  private contacto:any = {
    idContacto: 0,
    idCategoria: 0,
    nombre: "",
    apellido: "",
    direccion: "",
    telefono: "",
    correo: ""
  }
  private categorias:any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public contactoService: ContactoService
  ) {
    this.parametro = this.navParams.get('parametro');

    if(this.parametro != "nuevo") {
      this.contactoService.getContacto(this.parametro)
      .subscribe(res => this.contacto = res);
      this.titulo = "Detalle Contacto"
    } else {
      this.titulo = "Nuevo Contacto";
    }
  }

  ngOnInit() {
    this.contactoService.getCategorias().subscribe(data => {
      this.categorias = data;
    });
  }

  public guardar() {
    if(this.parametro != "nuevo") {
      this.contactoService.editarContacto(this.contacto)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(ContactosPage);
          }
        }, 3000);
      });
    } else {
      this.contactoService.nuevoContacto(this.contacto)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(ContactosPage);
          } else {
            this.contacto.nombre = "";
            this.contacto.apellido = "";
            this.contacto.direccion = "";
            this.contacto.telefono = "";
            this.contacto.correo = "";
            this.contacto.idCategoria = 0;
          }
        }, 3000);
      });
    }
  }
}
