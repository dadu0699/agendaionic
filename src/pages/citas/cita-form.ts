import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { CitaService } from '../../app/services/CitaService';
import { CitasPage } from './citas';

@Component({
  selector: 'page-cita-form',
  templateUrl: 'cita-form.html',
})
export class CitaFormPage implements OnInit {
  private parametro:string;
  private titulo:string;

  private cita:any = {
    idCita: 0,
    idContacto: 0,
    titulo: "",
    lugar: "",
    fecha: "",
  }
  private contactos:any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public citaService: CitaService
  ) {
    this.parametro = this.navParams.get('parametro');

    if(this.parametro != "nuevo") {
      this.citaService.getCita(this.parametro)
      .subscribe(res => this.cita = res);
      this.titulo = "Detalle Cita"
    } else {
      this.titulo = "Nuevo Cita";
    }
  }

  ngOnInit() {
    this.citaService.getContactos().subscribe(data => {
      this.contactos = data;
    });
  }

  public guardar() {
    if(this.parametro != "nuevo") {
      this.citaService.editarCita(this.cita)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(CitasPage);
          }
        }, 3000);
      });
    } else {
      this.citaService.nuevaCita(this.cita)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(CitasPage);
          } else {
            this.cita.idContacto = 0;
            this.cita.titulo = "";
            this.cita.lugar = "";
            this.cita.fecha = "";
          }
        }, 3000);
      });
    }
  }
}
