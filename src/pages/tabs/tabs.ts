import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ContactosPage } from '../contactos/contactos';
import { TareasPage } from '../tareas/tareas';
import { CitasPage } from '../citas/citas';
import { LoginPage } from '../login/login';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ContactosPage;
  tab2Root = TareasPage;
  tab3Root = CitasPage;
  //tab4Root = this.salir();

  constructor(
    public navCtrl: NavController
  ) {
  }

  salir() {
    localStorage.removeItem('TOKEN');
    this.navCtrl.setRoot(LoginPage);
  }
}
