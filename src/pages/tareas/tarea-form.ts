import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { TareaService } from '../../app/services/TareaService';
import { TareasPage } from './tareas';

@Component({
  selector: 'page-tarea-form',
  templateUrl: 'tarea-form.html',
})
export class TareaFormPage implements OnInit {
  private parametro:string;
  private titulo:string;

  private tarea:any = {
    idTarea: 0,
    titulo: "",
    descripcion: "",
    idEstado: 1,
    fechaFin: "",
  }
  private estados:any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public tareaService: TareaService
  ) {
    this.parametro = this.navParams.get('parametro');

    if(this.parametro != "nuevo") {
      this.tareaService.getTarea(this.parametro)
      .subscribe(res => this.tarea = res);
      this.titulo = "Detalle Tarea"
    } else {
      this.titulo = "Nuevo Tarea";
    }
  }

  ngOnInit() {
    this.tareaService.getEstados().subscribe(data => {
      this.estados = data;
    });
  }

  public guardar() {
    if(this.parametro != "nuevo") {
      this.tareaService.editarTarea(this.tarea)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(TareasPage);
          }
        }, 3000);
      });
    } else {
      this.tareaService.nuevaTarea(this.tarea)
      .subscribe(res => {
        this.toast.create({
          message: res.mensaje,
          duration: 2000
        }).present();

        setTimeout(() => {
          if(res.estado) {
            this.navCtrl.setRoot(TareasPage);
          } else {
            this.tarea.titulo = "";
            this.tarea.descripcion = "";
            this.tarea.idEstado = 0;
            this.tarea.fechaFin = "";
          }
        }, 3000);
      });
    }
  }
}
